# README #

This is an example project used for learning GIT.

### How to get started ###

* Press the "clone" link in the actions menu.
* If you are using SourceTree, press the "Clone in SourceTree" button and follow the guidelines.
* If you are using command line git, copy the SSH or HTTPS link and execute the command in your terminal.
* Check that the source files exist in the correct folder. Compare it to the "Source" section in the navigation menu.
* Check the commit history in SourceTree and compare it to the "Commits" section in the navigation menu.